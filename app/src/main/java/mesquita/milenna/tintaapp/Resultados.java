package mesquita.milenna.tintaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Resultados extends AppCompatActivity {


    TextView qtdTintas;
    TextView qtdMassa;
    TextView qtdSeladora;
    TextView mediaCusto;
    TextView returnCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        initializeUi();

        TintaAppApplication tintaAppApplication =   (TintaAppApplication) getApplicationContext();

        DecimalFormat df = new DecimalFormat("0.00");

        qtdMassa.setText(String.valueOf(tintaAppApplication.qtdMassas));
        qtdSeladora.setText(String.valueOf(tintaAppApplication.qtdSeladora));
        qtdTintas.setText(String.valueOf(tintaAppApplication.qtdTintas));
        mediaCusto.setText(String.valueOf(df.format(tintaAppApplication.somaGastos)));



        returnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void initializeUi(){

        qtdMassa = findViewById(R.id.act_resultados_massa);
        qtdTintas = findViewById(R.id.act_resultados_tinta);
        qtdSeladora = findViewById(R.id.act_resultados_seladora);
        mediaCusto = findViewById(R.id.act_resultados_custo);
        returnCalc = findViewById(R.id.act_resultados_return);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
