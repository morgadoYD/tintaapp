package mesquita.milenna.tintaapp;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;


/*
Entrada de dados: altura da parede, largura da parede, quantidade de portas, quantidade de janelas, quantidade de demãos, tipo de tinta, parede crua (sim ou não)

Saída de dados: quantidade necessária de tinta, quantidade de massa e seladora (caso seja parede crua), média de custo





*CASO SEJA NECESSÁRIO SELADORA E MASSA


 */

public class MainActivity extends AppCompatActivity {

    EditText altura;
    EditText largura;
    EditText numPortas;
    EditText numJanelas;
    EditText numDemaos;
    RadioButton acrilica;
    RadioButton plastica;
    RadioButton texturizada;
    RadioButton corrida;
    RadioButton seladora;
    RadioButton paredeCrua;
    Button calcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUi();

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calcular();

            }
        });

        acrilica .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (acrilica.getTag().toString().equals("false")){
                    acrilica.setChecked(true);
                    acrilica.setTag("true");
                }else{
                    acrilica.setChecked(false);
                    acrilica.setTag("false");
                }


                plastica.setChecked(false);
                    texturizada.setChecked(false);
                //    paredeCrua.setChecked(false);


            }
        });

        plastica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (plastica.getTag().toString().equals("false")){
                    plastica.setChecked(true);
                    plastica.setTag("true");
                }else{
                    plastica.setChecked(false);
                    plastica.setTag("false");
                }


                    acrilica.setChecked(false);
             //       paredeCrua.setChecked(false);
                    texturizada.setChecked(false);

            }
        });

        texturizada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (texturizada.getTag().toString().equals("false")){
                    texturizada.setChecked(true);
                    texturizada.setTag("true");
                }else{
                    texturizada.setChecked(false);
                    texturizada.setTag("false");
                }

                acrilica.setChecked(false);
                plastica.setChecked(false);
               //     paredeCrua.setChecked(false);

            }
        });

        /*

        paredeCrua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acrilica.setChecked(false);
                plastica.setChecked(false);
                texturizada.setChecked(false);
            }
        });

        */

        corrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("corrida","clicked " + corrida.isChecked());

                if (corrida.getTag().toString().equals("false")){
                    corrida.setChecked(true);
                    corrida.setTag("true");
                }else{
                    corrida.setChecked(false);
                    corrida.setTag("false");
                }

            }
        });

        seladora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seladora.getTag().toString().equals("false")){
                    seladora.setChecked(true);
                    seladora.setTag("true");
                }else{
                    seladora.setChecked(false);
                    seladora.setTag("false");
                }
            }
        });

    }

    private void initializeUi(){

        altura = findViewById(R.id.act_main_altura);
        largura = findViewById(R.id.act_main_largura);
        numPortas = findViewById(R.id.act_main_portas);
        numJanelas = findViewById(R.id.act_main_janelas);
        numDemaos = findViewById(R.id.act_main_demaos);
        acrilica = findViewById(R.id.act_main_acrilica);
        plastica = findViewById(R.id.act_main_plastica);
        texturizada = findViewById(R.id.act_main_texturizada);
        corrida = findViewById(R.id.act_main_massa_corrida);
        seladora = findViewById(R.id.act_main_seladora);
      //  paredeCrua = findViewById(R.id.act_main_parede_crua);
        calcular = findViewById(R.id.act_main_calcular);

    }

    private void calcular(){

        TintaAppApplication tintaAppApplication =  (TintaAppApplication) getApplicationContext();

        tintaAppApplication.qtdTintas = 0.0;
        tintaAppApplication.qtdMassas = 0.0;
        tintaAppApplication.qtdSeladora = 0.0;
        tintaAppApplication.somaGastos = 0.0;


        if (altura.getText().toString().isEmpty())
            return;
        if (largura.getText().toString().isEmpty())
            return;
        if (numPortas.getText().toString().isEmpty())
            return;
        if (numJanelas.getText().toString().isEmpty())
            return;
        if (numDemaos.getText().toString().isEmpty())
            return;

        /*

        Fórmulas: Área da parede= altura da parede×largura da parede
        Área total= Área da parede×(1,5×numero de portas+1,5×numero de janelas)
        Redimentos
         */

        /*
        total=(4×area da parede) - (1,5× número de portas + 1,5× número de janelas
         */


         Double auxAreaParede =  Double.parseDouble( altura.getText().toString()) * Double.parseDouble( largura.getText().toString());
         Double auxAreaTotal = (4*auxAreaParede) - ((1.5 *Integer.parseInt(numPortas.getText().toString())) + (1.5 * Integer.parseInt(numJanelas.getText().toString())));

         /*
  *PARA TINTAS ACRÍLICAS
        quantidade de litros necessários= área total÷12
                *PARA TINTAS PLÁSTICAS
        quantidade de litros necessários= área total÷7,5
                *PARA TINTAS TEXTURIZADAS
        quantidade de litros necessários= área total÷15

        *CÁLCULO DO PREÇO DA TINTA

        -ACRILICA
        Gasto= 12×quantidade de tinta necessaria
        -PLASTICA
        Gasto= 3×quantidade de tinta necessaria
        -TEXTURIZADAS
        Gasto= 17×quantidade de tinta necessaria
        */

          String tag = "Soma Gastos";

         if (acrilica.isChecked()){
             Double qtdLitros = Double.parseDouble(numDemaos.getText().toString()) * (auxAreaTotal / 12);
             tintaAppApplication.qtdTintas = qtdLitros;
             tintaAppApplication.somaGastos = qtdLitros * 12;

         }

         if (plastica.isChecked()){
             Double qtdLitros = Double.parseDouble(numDemaos.getText().toString()) * (auxAreaTotal / 7.5);
             tintaAppApplication.qtdTintas = qtdLitros;
             tintaAppApplication.somaGastos = qtdLitros * 3;
         }

        if (texturizada.isChecked()){
            Double qtdLitros = Double.parseDouble(numDemaos.getText().toString()) * (auxAreaTotal / 15);
            tintaAppApplication.qtdTintas = qtdLitros;
            tintaAppApplication.somaGastos = qtdLitros * 17;
        }

  //      Log.i(tag," "+tintaAppApplication.somaGastos);
        /*
        *PREÇO DA MASSA E DA SELADORA
        Gasto com a massa = (quantidade de massa necessária × 70) ÷ 28
        Gasto com a seladora = (quantidade de seladora ×110)÷ 18
        */

        if (seladora.isChecked() && corrida.isChecked()){

            Double qtdSeladora = ((auxAreaTotal * 18 ) / 120);
            Double qtdMassa = ((auxAreaTotal * 18 ) / 200);


            tintaAppApplication.qtdSeladora = qtdSeladora;
            tintaAppApplication.qtdMassas = qtdMassa;

            tintaAppApplication.somaGastos = tintaAppApplication.somaGastos + ((qtdMassa * 70) / 28) + (( qtdSeladora * 110) / 18);

            Log.i(tag,"Qtd tinta: " + tintaAppApplication.qtdTintas + " Qtd Massa: " + qtdMassa +  " Qtd Seladora :" + qtdSeladora + " Media: "+tintaAppApplication.somaGastos);

        }else{
            if (seladora.isChecked()){
                Double qtdSeladora = ((auxAreaTotal * 18 ) / 120);
                tintaAppApplication.qtdSeladora = qtdSeladora;
                tintaAppApplication.somaGastos =+ (( qtdSeladora * 110) / 18);
            }else
                tintaAppApplication.qtdSeladora = 0.0;

            if (corrida.isChecked()){
                Double qtdMassa = ((auxAreaTotal * 18 ) / 200);
                tintaAppApplication.qtdMassas = qtdMassa;
                tintaAppApplication.somaGastos =+ ((qtdMassa * 70) / 28);
            }else
                tintaAppApplication.qtdMassas = 0.0;
        }
/*
        quantidade de seladora necessária= (18×area total) ÷ 120
        quantidade de massa necessária= (18×area total) ÷ 200

        *PREÇO DA MASSA E DA SELADORA

Gasto com a seladora = (quantidade de seladora ×110)÷ 18

Gasto com a massa = (quantidade de massa necessária × 70) ÷ 28

*/



        startActivity(new Intent(getApplicationContext(),Resultados.class));

    }
}